import React, {Component} from 'react';
import {RealTimeMetricsGraph} from "../metrics_graph/RealTimeMetricsGraph";
import {MeterList} from "../meters_list/MetersList";
import {MeterService} from "../../service/MeterService";

export class Metrics extends Component {


    constructor(props) {
        super(props);
        this.state = {meterFilter: []}
    }

    componentDidMount() {
        MeterService.instance.getMeters().then(
            resp => {
                if (resp) {
                    console.log(resp);
                    let filter = resp.map(m => ({id: m, active: false}));
                    if (filter[0]) filter[0].active = true;
                    this.setState({meterFilter: filter})
                    this.onMetricChange(filter[0], true);
                }
            }
        )
    }

    onMetricChange = (metricFilterVal, status) => {
        let metricFilterCopy = this.state.meterFilter.slice();
        let idx = metricFilterCopy.findIndex(mf => mf.id === metricFilterVal.id);
        metricFilterCopy[idx].active = status;
        this.setState({meterFilter: metricFilterCopy})
    };

    render() {
        return <div>
            <MeterList allFilters={this.state.meterFilter} onFilterChange={this.onMetricChange}/>
            {(this.state.meterFilter.length) && <RealTimeMetricsGraph metricFilter={this.state.meterFilter}/>}
        </div>
    }
}