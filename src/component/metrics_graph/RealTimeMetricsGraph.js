import React, {Component} from 'react';
import {METRICS_URL} from "../../const/const";
import * as Chart from 'chart.js';
import {WebSocketConnector} from '../../service/WebSocketConnector';
import {colors} from "../../const/colors";

export class RealTimeMetricsGraph extends Component {

    constructor(props) {
        super(props);
        this.subscriptions = {};
        this.allMeters = {};
    }

    componentDidMount = () => {
        this.props.metricFilter.forEach(f => this.allMeters[f.id] = []);
    };

    componentDidUpdate = (prevProps) => {
        let self = this;
        let filter = self.props.metricFilter;
        let activeMeters = filter.filter(f => f.active);
        this.activeMetersIds = activeMeters.map(f => f.id);

        Object.entries(filter).forEach(meter => {
            let meterId = meter[0];
            self.subscriptions[meterId] = self.subscribe(meterId, reading => {
                this.handleNewReading(reading, meterId);
                if (self.activeMetersIds.includes(meterId)) {
                    self.updateChart(meterId);
                }
            });
            return meter
        });

        this.initializeChartCanvas(
            Object.entries(self.allMeters)
                .filter(f => self.activeMetersIds.includes(f[0]))
        );
    };


    initializeChartCanvas(datasets) {
        this.last = Date.now();
        let ctx = document.getElementById('real_time_metrics').getContext('2d');
        this.myChart && this.myChart.destroy();
        this.myChart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: datasets
                    .map(meterDataSet => {
                        let meterId = meterDataSet[0];
                        let data = meterDataSet[1];
                        return RealTimeMetricsGraph.convertDataset(meterId, data)
                    }),
            },
            options: {
                elements: {
                    line: {
                        tension: 0.2 // disables bezier curves
                    }
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            bounds: 'ticks',
                            unit: 'second',
                            stepSize: 10,
                            min: new Date(Date.now()).minusMinutes(1),
                            max: new Date(Date.now()),
                            displayFormats: {
                                quarter: 'h:mm a'
                            }
                        },
                        position: 'bottom'
                    }]
                }
            }
        });
    }

    subscribe(meterId, callback) {
        if (this.subscriptions[meterId]) return this.subscriptions[meterId];
        return WebSocketConnector.instance
            .subscribeTo(METRICS_URL(meterId))
            .subscribe(callback);
    }

    static convertDataset(meterId, dataPoints) {
        return {
            id: meterId,
            borderColor: colors[+meterId],
            label: `Meter Id: ${meterId}`,
            fill: false,
            data: dataPoints
        }
    }

    static convertDatasetData = (readings) => {
        return readings.map(RealTimeMetricsGraph.convertData);
    };

    static convertData(el) {
        return ({
            x: new Date(el.timestamp),
            y: el.data.power
        })
    }

    handleNewReading = (reading, meterId) => {
        let dataset = this.allMeters[meterId];
        if (dataset) {
            if (dataset.length > 20) {
                dataset.splice(0, 1);
            }
            dataset.push(RealTimeMetricsGraph.convertData(reading));
        }
    };

    findDatasetWithId = (meterId) => {
        let allDatasets = this.myChart.data.datasets;
        return allDatasets.find(ds => ds.id === meterId)
    };

    updateChart = (meterId, dataPoint) => {
        // let dataset = RealTimeMetricsGraph.convertDataset(meterId, this.allMeters[meterId]);
        this.updateChartLine(meterId);
        this.updateChartRange();
        this.myChart.update();
    };

    updateChartLine = (meterId) => {
        let dataset = this.findDatasetWithId(meterId);
        dataset.data = this.allMeters[meterId];
    };

    updateChartRange = () => {
        let ticks = this.myChart.options.scales.xAxes[0].time;
        let now = Date.now();
        let delta = now - this.last;
        ticks.min.setTime(ticks.min.getTime() + delta);
        ticks.max.setTime(ticks.max.getTime() + delta);
        this.last = now;
    };

    getTooltipPositionConfiguration = () => {
        Chart.Tooltip.positioners.custom = function (elements, eventPosition) {
            var tooltip = this;
            return {
                x: 0,
                y: 0
            };
        };
    };

    render() {
        return <div className="real_time_metrics_wrapper">
            <canvas id="real_time_metrics" width="100px" height="100px"/>
        </div>
    }

}