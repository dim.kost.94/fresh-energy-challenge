import React from 'react';
import {TotalEnergy} from "../total_energy/TotalEnergy";

export function MeterTrigger(props) {
    function onChange() {
        props.onMetricValueChange(props.metrFilter, !props.metrFilter.active);
    }

    return <div>
        <label>Meter {props.metrFilter.id}</label>
        <input type="checkbox" checked={props.metrFilter.active} onChange={onChange}/>
        <TotalEnergy meterId={props.metrFilter.id}/>
    </div>
}