import React, {Component} from 'react';
import {WebSocketConnector} from "../../service/WebSocketConnector";
import {CONTROLS_METRICS_URL} from "../../const/const";

export class TotalEnergy extends Component {

    constructor(props) {
        super(props);
        this.state = {totalPower: 0};
    }

    componentDidMount() {
        WebSocketConnector.instance.subscribeTo(CONTROLS_METRICS_URL(this.props.meterId))
            .subscribe(controll => {
                this.setState({totalPower: controll.sum});
            })
    }

    render() {
        return <div className="total_energy">{this.state.totalPower}</div>
    }
}