import React from 'react';
import {MeterTrigger} from "../metrics_trigger/MetricksTrigger";

export function MeterList(props) {

    function getMetersTriggers() {
        console.log(props.allMeters);
        if (props.allFilters) {
            return props.allFilters.map((m, i) =>
                <div key={i}>
                    <MeterTrigger metrFilter={m} onMetricValueChange={props.onFilterChange}/>
                </div>
            )
        }
    };

    return <div className={"meter-list-wrapper"}>
        {getMetersTriggers()}
    </div>

}