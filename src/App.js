import React, {Component} from 'react';
import './styles/App.css';
import {Metrics} from "./component/metrics/Metrics";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Metrics/>
            </div>
        );
    }
}

export default App;
