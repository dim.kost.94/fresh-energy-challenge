import * as SockJs from 'sockjs-client';
import * as STOMP from 'stompjs';
import {WEB_SOCKET} from "../const/const";
import {Subject} from 'rxjs';

export class WebSocketConnector {

    static instance = new WebSocketConnector(WEB_SOCKET);

    constructor(connectionUrl) {
        this.connectionObservable = [];
        this.connect(connectionUrl);
        this.connectionObservable = new Subject();
    }

    connect = (connectionUrl) => {
        this.websocket = new SockJs(connectionUrl);
        this.stomp = STOMP.over(this.websocket);
        this.stomp.connect({}, () => {
            this.connectionObservable.next();
        }, () => {
            setTimeout(this.connect, 1000);
        });
    };

    subscribeTo = (topic) => {
        let meterSubject = new Subject();
        this.connectionObservable
            .subscribe(() =>
                this.stomp.subscribe(topic, (message => {
                    let reading = JSON.parse(message.body);
                    meterSubject.next(reading);
                }))
            );
        return meterSubject;
    }


}