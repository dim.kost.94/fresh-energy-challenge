import {BASE_URL} from "../const/const";

export const DEFAULT_PAGE_SIZE = 100;

export class MeterService {

    static instance = new MeterService();

    getMeters = () => fetch(`${BASE_URL}/api/meters`,
        {
            headers: {
                "accepts": "application/json"
            }
        }).then(res => {
            console.log(res);
            return res.json();
        }
    );

    getMetersReadings = (pageSize = DEFAULT_PAGE_SIZE) =>
        fetch(`${BASE_URL}/api/meters/readings?pageSize=${pageSize}`,
            {
                headers: {
                    "accepts": "application/json"
                }
            }).then(res => {
                console.log(res);
                return res.json();
            }
        )

}
