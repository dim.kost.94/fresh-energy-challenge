export const HOST_NAME = "https://4e3be768.ngrok.io";
export const WEB_SOCKET = HOST_NAME + "/web";
export const METRICS_URL = (meterId) => `/topic/meter/${meterId}/indicator`;
export const CONTROLS_METRICS_URL = (meterId) => `/topic/meter/${meterId}/control`;
export const BASE_URL = "http://localhost:8080";